package cmd

import (
	"fmt"
	"os"

	jira "github.com/andygrunwald/go-jira"
	"github.com/spf13/cobra"
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "rhjira",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(args)
	},
}

var JiraClient *jira.Client

func Execute(_client *jira.Client) {
	JiraClient = _client
	if err := RootCmd.Execute(); err != nil {
		// Execute has already logged the error
		os.Exit(1)
	}
}

func init() {
}
